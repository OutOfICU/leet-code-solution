package com.leetcode.test;

import com.leetcode.search.SearchSolution;

public class TestSearchSolution {
    public static void main(String[] args) {
        SearchSolution solution = new SearchSolution();

        int[] array = new int[]{1,3,5,6};
        System.out.println(solution.searchInsert(array,5));


    }
}
