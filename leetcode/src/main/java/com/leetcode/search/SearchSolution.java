package com.leetcode.search;

public class SearchSolution {

    //35��������λ��
    public int searchInsert(int[] nums, int target) {
        int start = 0,end = nums.length-1,position=0;
        while (true){
            if (start>=end||nums[position]>=target){
                break;
            }
            int mid = (start+end)/2;
            if(nums[mid] <= target){
                start = mid;
            }else {
                end = mid;
            }
            position = start;

        }

        return position;
    }

}
