package com.leetcode.tree;

import com.leetcode.base.TreeNode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TreeSolution {


    public TreeNode buildTree(Integer[] preOrder,Integer[] inOrder){
        if(preOrder == null || inOrder==null){
            return null;
        }
        return buildTree(preOrder,inOrder,0,preOrder.length-1,0,inOrder.length-1);
    }

    public TreeNode buildTree(Integer[] preOrder,Integer[] inOrder,int ps,int pe,int is,int ie){
        Integer headVal = inOrder[is];
        TreeNode head = new TreeNode(headVal);
        int leftLen = 0;
        for (int i = ps;i<pe;i++){
            if(preOrder[i]==headVal){
                break;
            }
            leftLen++;
        }
//        int rightLen = pe-ps-leftLen;
        if(ie>is && leftLen>0){
            head.left = buildTree(preOrder,inOrder,ps,ps+leftLen-1,is+1,is+leftLen);
        }
        if (pe>ps){
            head.right = buildTree(preOrder,inOrder,ps+leftLen+1,pe,is+leftLen+1,ie);
        }

        return head;
    }

    //打印二叉树
    public void printTree(TreeNode tree){
        int maxLevel = maxDepth(tree);

        printNodeInternal(Collections.singletonList(tree), 1, maxLevel);
    }

    void printNodeInternal(List<TreeNode> nodes, int level, int maxLevel) {
        if (nodes.isEmpty() || isAllElementsNull(nodes)) {
            return;
        }
        int floor = maxLevel - level;
        int edgeLines = (int) Math.pow(2, Math.max(floor - 1, 0));
        int firstSpaces = (int) Math.pow(2, floor) - 1;
        int betweenSpaces = (int) Math.pow(2, floor + 1) - 1;

        printWhitespaces(firstSpaces);

        List<TreeNode> newNodes = new ArrayList<>();
        for (TreeNode node : nodes) {
            if (node != null) {
                System.out.print(node.val);
                newNodes.add(node.left);
                newNodes.add(node.right);
            } else {
                newNodes.add(null);
                newNodes.add(null);
                System.out.print(" ");
            }

            printWhitespaces(betweenSpaces);
        }
        System.out.println();

        for (int i = 1; i <= edgeLines; i++) {
            for (int j = 0; j < nodes.size(); j++) {
                printWhitespaces(firstSpaces - i);
                if (nodes.get(j) == null) {
                    printWhitespaces(edgeLines + edgeLines + i + 1);
                    continue;
                }

                if (nodes.get(j).left != null) {
                    System.out.print("/");
                } else {
                    printWhitespaces(1);
                }

                printWhitespaces(i + i - 1);

                if (nodes.get(j).right != null) {
                    System.out.print("\\");
                } else {
                    printWhitespaces(1);
                }

                printWhitespaces(edgeLines + edgeLines - i);
            }

            System.out.println();
        }

        printNodeInternal(newNodes, level + 1, maxLevel);
    }

    // 打印空格
    void printWhitespaces(int count) {
        for (int i = 0; i < count; i++) {
            System.out.print(" ");
        }
    }

    // 检查节点列表是否全为空
    boolean isAllElementsNull(List<TreeNode> nodes) {
        for (TreeNode node : nodes) {
            if (node != null) {
                return false;
            }
        }
        return true;
    }


    //94 二叉树中序遍历
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> retList = new ArrayList<>();
        inorder(root,retList);
        return retList;

    }

    public void inorder(TreeNode root,List<Integer> retList){
        if (root == null){
            return;
        }
        inorder(root.left,retList);
        retList.add(root.val);
        inorder(root.right,retList);
    }

    //104 二叉树的最大深度
    public int maxDepth(TreeNode root) {
        if(root == null){
            return 0;
        }
        int leftDeep = maxDepth(root.left);
        int rightDeep = maxDepth(root.right);
        return leftDeep>rightDeep?leftDeep+1:rightDeep+1;
    }

    //226反转二叉树
    public TreeNode invertTree(TreeNode root) {
        if (root==null){
            return null;
        }
        invertTree(root.left);
        invertTree(root.right);
        TreeNode temp = root.left;
        root.left=root.right;
        root.right = temp;
        return root;
    }

//            if (root==null){
//        return null;
//    }
//    TreeNode left = invertTree(root.left);
//    TreeNode right = invertTree(root.right);
//    root.left=right;
//    root.right = left;


    //101对称二叉树
    public boolean isSymmetric(TreeNode root) {

        if(root == null){
            return true;
        }
        return dfs(root.left,root.right);
    }

    private boolean dfs(TreeNode left, TreeNode right) {
        if (right==null && left==null){
            return true;
        }
        if (right==null || left==null){
            return false;
        }
        if (left.val != right.val){
            return false;
        }

        return dfs(left.right,right.left)&&dfs(left.left,right.right);
    }


    public int diameterOfBinaryTree(TreeNode root) {

        return 0;
    }

    //108 有序数组转换为二叉搜索数
    public TreeNode sortedArrayToBST(int[] nums) {
        return helper(nums,0,nums.length-1);
    }

    public TreeNode helper(int[] nums,int left, int right){
        if (left>right){
            return null;
        }
        int mid = (left+right)/2;
        TreeNode node = new TreeNode(nums[mid]);
        node.left = helper(nums,left,mid-1);
        node.right = helper(nums,mid+1,right);
        return node;
    }


}
